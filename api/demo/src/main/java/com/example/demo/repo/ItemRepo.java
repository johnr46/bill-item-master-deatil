package com.example.demo.repo;

import com.example.demo.domain.Item;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ItemRepo extends JpaRepository<Item, Integer> {
            
        // @Query( value = 
        //     "DELETE i FROM item i WHERE i.id = :id",nativeQuery = true
        // )
        // void deleteById(@Param("id") int id);
}