package com.example.demo.repo;

import java.util.List;

import com.example.demo.domain.Bill;
import com.example.demo.dto.BillCriteriaDTO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BillRepo extends JpaRepository<Bill, Integer> {

    List<Bill> findByBillCode(String billCode);

    @Query("SELECT b FROM Bill b   WHERE 1=1 "
            + " AND ((:#{#p.billCode} IS NULL)  OR  (b.billCode LIKE (CONCAT('%',TRIM(:#{#p.billCode}),'%')) ) ) "
    // "SELECT b FROM Bill b LEFT JOIN FETCH b.itemList item WHERE 1=1 "
    // + " AND ((:#{#p.billCode} IS NULL) OR (b.billCode LIKE
    // (CONCAT('%',TRIM(:#{#p.billCode}),'%')) ) ) "
    // + "AND ("
    // + " ((:#{#p.name} IS NULL ) OR ( item.name LIKE CONCAT('%',
    // TRIM(:#{#p.name}), '%') ) ) "
    // + " OR ((:#{#p.qty} IS NULL ) OR ( item.qty = :#{#p.qty} ) ) "
    // + " OR ((:#{#p.price} IS NULL ) OR ( item.price = :#{#p.price} ) ) "
    // + ")"
    )
    List<Bill> findByCriteria(@Param("p") BillCriteriaDTO billCri);


 
    @Query(value = "SELECT COUNT(b) FROM bill b WHERE  b.id != :id AND ( (b.bill_code = :code ) ) ",nativeQuery = true)
     int CheckHaveBill(@Param ("id")int id, @Param("code") String code);
}


