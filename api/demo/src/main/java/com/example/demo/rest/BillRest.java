package com.example.demo.rest;

import java.util.List;

import com.example.demo.domain.Bill;
import com.example.demo.domain.Item;
import com.example.demo.dto.BillCriteriaDTO;
import com.example.demo.repo.BillRepo;
import com.example.demo.repo.ItemRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/bills")
@Slf4j
public class BillRest {

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private ItemRepo itemRepo;

    @GetMapping("/find-all")
    public ResponseEntity<?> findAll() {
        try {
            List<Bill> bills = this.billRepo.findAll();
            return ResponseEntity.ok().body(bills);
        } catch (Exception e) {
            return ResponseEntity.status(420).body("เกิดข้อผิดพลาด ");
        }
    }

    @PostMapping(value = "/delete")
    public ResponseEntity<?> delete(@RequestBody Bill body) {
        try {

            List<Item> items = body.getItemList();

            for (Item item : items) {
                this.itemRepo.delete(item);
            }
            this.billRepo.delete(body);

            return ResponseEntity.ok().body(body);
        } catch (Exception e) {
            log.info("delete error " + e.getMessage());
            return ResponseEntity.status(420).body("ลบไม่สำเร็จ");
        }
    }

    @PostMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody Bill body) {

        try {
            body.setActive(0);
            log.info("bill Code  : " + body.getBillCode());
            List<Item> items = body.getItemList();

            int billHaveing = this.billRepo.CheckHaveBill(body.getId(), body.getBillCode());
            log.info("billhaveing : " + billHaveing);

            if (billHaveing >= 1) {
                return ResponseEntity.status(420).body("รหัสของ bill ซ้ำกัน");
            } else if (items.isEmpty()) {
                return ResponseEntity.status(420).body("กรุณาใส่รายการไอเท็มอย่างน้อย 1 รายการ");

            } else {
                this.billRepo.save(body);
                for (Item item : items) {
                    item.setBill(body);
                    this.itemRepo.save(item);
                }
                body.setItemList(items);
                return ResponseEntity.ok().body(body);
            }

        } catch (Exception e) {
            return ResponseEntity.status(420).body("ติดต่อผู้ดูแลระบบ");
        }
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Bill body) {

        try {
            log.info("bill Code  : " + body.getBillCode());

            List<Bill> bills = this.billRepo.findByBillCode(body.getBillCode());
            log.info("size bills  : " + bills.size());
            List<Item> items = body.getItemList();
            if (bills.size() >= 1) {
                return ResponseEntity.status(420).body("รหัสของ bill ซ้ำกัน");
            } else if (items.isEmpty()) {
                return ResponseEntity.status(420).body("กรุณาใส่รายการไอเท็มอย่างน้อย 1 รายการ");

            } else {
                body.setId(null);
                this.billRepo.save(body);
                for (Item item : items) {
                    item.setBill(body);
                    this.itemRepo.save(item);
                }
                body.setItemList(items);
                return ResponseEntity.ok().body(body);
            }

        } catch (Exception e) {
            return ResponseEntity.status(420).body(e.getMessage());
        }
    }

    @PostMapping("/find-by-criteria")
    public ResponseEntity<?> findByCriteria(@RequestBody BillCriteriaDTO billCri) {
        log.info(" bill : " + billCri.getBillCode());

        try {
            List<Bill> bills = this.billRepo.findByCriteria(billCri);
            return ResponseEntity.status(200).body(bills);

        } catch (Exception e) {
            log.info(e.getMessage());
            return ResponseEntity.status(420).body("เกิดข้อผิดพลาด");
        }
    }

}