package com.example.demo.dto;

import lombok.ToString;

@ToString
public class BillCriteriaDTO {

    private String billCode;
    private String name;
    private Integer qty;
    private Integer price;

    /**
     * @return String return the billCode
     */
    public String getBillCode() {
        if (this.billCode == null || "".equals(this.billCode)) {
            return "";
        }
        return billCode;
    }

    /**
     * @param billCode the billCode to set
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * @return String return the name
     */
    public String getName() {
        if (this.name == null || "".equals(this.name)) {
            return "";
        }
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return String return the qty
     */
    public Integer getQty() {

        if (this.qty == null || "".equals(this.qty)) {
            return null;
        }
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * @return Integer return the price
     */
    public Integer getPrice() {

        if (this.price == null || "".equals(this.price)) {
            return null;
        }
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

}