import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { Item } from '../../types/item';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnInit {
  @Input() canEdit: any;
  @Input()
  set formValue(item: Item) {
    this.item.patchValue(item || []);
  }
  public setMode: boolean;

  @Output() addItem = new EventEmitter<Item>();
  @Output() update = new EventEmitter<Item>();

  item: FormGroup = this.fb.group({
    id: [undefined],
    name: [undefined, [Validators.maxLength(150)]],
    qty: [undefined, [Validators.pattern('^[0-9]*$')]],
    price: [undefined, [Validators.pattern('^[0-9]*$')]]
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    console.log(this.canEdit);

    if (this.canEdit === 'CREATE') {
      this.setMode = true;
      this.item.enable();
    } else {
      this.setMode = false;
      this.item.disable();
    }
  }

  get qty(): FormControl {
    return this.item.get('qty') as FormControl;
  }

  get name(): FormControl {
    return this.item.get('name') as FormControl;
  }

  get price(): FormControl {
    return this.item.get('price') as FormControl;
  }

  checkPriceValidate() {
    if (this.price.hasError('required')) {
      return 'กรุณาใส่ราคา';
    } else if (this.price.hasError('pattern')) {
      return 'เป็นตัวเลข';
    } else {
      return '';
    }
  }

  checkQtyValidate() {
    if (this.qty.hasError('required')) {
      return 'กรุณาใส่จำนวนของ';
    } else if (this.qty.hasError('pattern')) {
      return 'เป็นตัวเลข';
    } else {
      return '';
    }
  }
  checkNameValidate() {
    if (this.name.hasError('required')) {
      return 'กรุณาใส่ชื่อไอเท็ม';
    } else if (this.name.hasError('maxlength')) {
      return 'ยาวไม่เกิน 150 ตัวอักษร';
    } else {
      return '';
    }
  }
  toAddItem(): void {
    if (this.item.invalid) {
    } else {
      const val: Item = this.item.getRawValue();
      if (!val.id) {
        this.addItem.emit(val);
        this.item.reset();
      } else {
        this.update.emit(val);
        this.item.reset();
      }
    }
  }
}
