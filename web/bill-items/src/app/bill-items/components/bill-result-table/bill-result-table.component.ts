import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Bill } from '../../types/bill';
import { ColumnMode } from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-bill-result-table',
  templateUrl: './bill-result-table.component.html',
  styleUrls: ['./bill-result-table.component.scss']
})
export class BillResultTableComponent implements OnInit {
  @Input() resultSearch: Bill[] = [];

  @Output() update = new EventEmitter<Bill>();
  @Output() view = new EventEmitter<Bill>();

  setActiveNotUpdate: boolean;
  constructor() {}
  ColumnMode = ColumnMode.force;

  ngOnInit() {}

  statueBill(s: number): string {
    if (s === 1) {
      this.setActiveNotUpdate = true;
      return 'ปกติ';
    } else {
      this.setActiveNotUpdate = false;
      return 'รายการนี้ถูกยกเลิกไปแล้ว';
    }
  }
}
