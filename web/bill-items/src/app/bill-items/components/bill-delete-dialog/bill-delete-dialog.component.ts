import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DeleteConfirm } from './delete-confirm';

@Component({
  selector: 'app-bill-delete-dialog',
  templateUrl: './bill-delete-dialog.component.html',
  styleUrls: ['./bill-delete-dialog.component.scss']
})
export class BillDeleteDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<BillDeleteDialogComponent>,
    private fb: FormBuilder
  ) {}

  inactiveReason = new FormControl('', Validators.required);

  ngOnInit() {}

  onOk(): void {
    this.inactiveReason.markAsTouched();
    if (this.inactiveReason.valid) {
      const result: DeleteConfirm = {
        confirm: true,
        inactiveReason: this.inactiveReason.value
      };
      this.dialogRef.close(result);
    }
  }

  onCancel(): void {
    this.closeDialog();
  }

  closeDialog(): void {
    const result: DeleteConfirm = {
      confirm: false
    };
    this.dialogRef.close(result);
  }
}
