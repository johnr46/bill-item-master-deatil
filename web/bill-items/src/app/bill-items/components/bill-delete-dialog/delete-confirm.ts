export interface DeleteConfirm {
  confirm: boolean;
  inactiveReason?: string;
}
