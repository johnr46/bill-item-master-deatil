import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../../types/item';
import { ColumnMode } from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-item-table',
  templateUrl: './item-table.component.html',
  styleUrls: ['./item-table.component.scss']
})
export class ItemTableComponent implements OnInit {
  @Input() canEdit: any;
  @Input() itemList: Item[] = [];

  @Output() update = new EventEmitter<Item>();
  @Output() delete = new EventEmitter<Item>();

  public setMode: boolean;
  constructor() {}

  ColumnMode = ColumnMode.force;
  ngOnInit() {
    if (this.canEdit === 'VIEW' || this.canEdit === 'UPDATE') {
      this.setMode = false;
    } else {
      this.setMode = true;
    }
  }
}
