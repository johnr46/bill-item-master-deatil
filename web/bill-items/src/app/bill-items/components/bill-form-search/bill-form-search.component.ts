import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BillCriteria } from '../../types/billcriteria';

@Component({
  selector: 'app-bill-form-search',
  templateUrl: './bill-form-search.component.html',
  styleUrls: ['./bill-form-search.component.scss']
})
export class BillFormSearchComponent implements OnInit {
  @Input() formCriteria: BillCriteria;
  @Output() billCri = new EventEmitter<BillCriteria>();
  @Output() Clear = new EventEmitter<void>();

  billCriteriaForm: FormGroup = this.fb.group({
    billCode: [undefined],
    name: [undefined],
    qty: [undefined],
    price: [undefined]
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.billCriteriaForm.patchValue(this.formCriteria || {});
  }

  toSearch(): void {
    const val: BillCriteria = this.billCriteriaForm.getRawValue();
    this.billCri.emit(val);
  }

  toClear(): void {
    this.billCriteriaForm.reset();
    this.Clear.emit();
  }
}
