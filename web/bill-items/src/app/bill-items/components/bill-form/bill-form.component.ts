import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import { Bill } from '../../types/bill';
import { Item } from '../../types/item';
import { MatDialog } from '@angular/material/dialog';
import { BillDeleteDialogComponent } from '../bill-delete-dialog/bill-delete-dialog.component';
import { DeleteConfirm } from '../bill-delete-dialog/delete-confirm';

@Component({
  selector: 'app-bill-form',
  templateUrl: './bill-form.component.html',
  styleUrls: ['./bill-form.component.scss']
})
export class BillFormComponent implements OnInit {
  @Input() formBillFromPage: Bill;
  @Output() save = new EventEmitter<Bill>();
  @Output() update = new EventEmitter<Bill>();

  canEdit: any;
  setView: boolean;
  setUpdate: boolean;
  setCreate: boolean;
  @Input()
  set setMode(state) {
    this.canEdit = state;
    if (state === 'VIEW') {
      this.setView = true;
      this.setUpdate = false;
      this.setCreate = false;
    } else if (state === 'UPDATE') {
      this.setView = false;
      this.setUpdate = true;
      this.setCreate = false;
    } else if (state === 'CREATE') {
      this.setView = false;
      this.setUpdate = false;
      this.setCreate = true;
    }
  }

  billForm: FormGroup = this.fb.group({
    id: [undefined],
    billCode: [
      undefined,
      [Validators.pattern('^[0-9]*$'), Validators.required]
    ],
    active: [1],
    inactiveReason: [undefined, Validators.required],
    itemList: [[]]
  });

  itemValue: Item;
  formValue: Item;

  constructor(private fb: FormBuilder, public dialog: MatDialog) {}

  ngOnInit() {
    this.billForm.patchValue(this.formBillFromPage || {});
    if (this.setView) {
      this.billForm.disable();
    }
    if (this.setUpdate) {
      this.billCode.disable();
      this.inactiveReason.enable();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BillDeleteDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((result: DeleteConfirm) => {
      console.log(result);
      if (result.confirm) {
        this.inactiveReason.setValue(result.inactiveReason);
        this.toUpdateBill();
      }
    });
  }

  reciveItem(item: Item): void {
    const lastIndex = this.itemListValue.length;
    const itemList = [...this.itemListValue, { ...item, id: null }];
    this.itemList.setValue(itemList);
  }

  reciveItemformTableDelete(item: Item): void {
    console.log('delete');
    const newitem: Item[] = [];
    for (let i of this.itemListValue) {
      if (i === item) {
        delete this.itemListValue[i.id];
      } else {
        newitem.push(i);
      }
    }
    this.itemList.setValue(newitem);
  }

  reciveItemUpdate(item: Item): void {
    console.log('update');
    const newitem: Item[] = [];
    this.itemListValue.filter(olditem => {
      if (olditem.id === item.id) {
        newitem.push(item);
      } else {
        newitem.push(olditem);
      }
    });
    this.itemList.setValue(newitem);
  }

  reciveItemformTable(item: Item): void {
    this.formValue = item;
  }
  get itemList(): FormControl {
    return this.billForm.get('itemList') as FormControl;
  }

  toSaveBill(): void {
    if (!this.billCode.invalid) {
      const val: Bill = this.billForm.getRawValue();
      this.save.emit(val);
    }
  }

  toUpdateBill(): void {
    const val: Bill = this.billForm.getRawValue();
    this.update.emit(val);
  }

  checkBillCode() {
    if (this.billCode.hasError('required')) {
      return 'กรุณาใส่รหัส';
    } else if (this.billCode.hasError('pattern')) {
      return 'เป็นตัวเลข';
    } else {
      return '';
    }
  }

  get inactiveReason(): FormControl {
    return this.billForm.get('inactiveReason') as FormControl;
  }

  checkActive() {
    if (this.active.value) {
      return false;
    } else {
      return true;
    }
  }

  get active(): FormControl {
    return this.billForm.get('active') as FormControl;
  }

  get billCode(): FormControl {
    return this.billForm.get('billCode') as FormControl;
  }

  get itemListValue(): Item[] {
    return this.itemList.value || [];
  }
}
