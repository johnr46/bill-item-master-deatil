import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { BillItemRoutingModule } from './bill-item-routing.module';
import { BillFormSearchComponent } from './components/bill-form-search/bill-form-search.component';
import { BillFormComponent } from './components/bill-form/bill-form.component';
import { BillResultTableComponent } from './components/bill-result-table/bill-result-table.component';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { ItemTableComponent } from './components/item-table/item-table.component';
import { BillFormPageComponent } from './containers/bill-form-page.component';
import { BillSearchPageComponent } from './containers/bill-search-page.component';
import { BillDeleteDialogComponent } from './components/bill-delete-dialog/bill-delete-dialog.component';
import { MatToolbarModule } from '@angular/material/toolbar';
@NgModule({
  declarations: [
    BillFormPageComponent,
    BillSearchPageComponent,
    BillFormComponent,
    ItemFormComponent,
    ItemTableComponent,
    BillFormSearchComponent,
    BillResultTableComponent,
    BillDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    BillItemRoutingModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    NgxDatatableModule,
    MatIconModule,
    MatDialogModule,
    MatToolbarModule
  ],
  entryComponents: [BillDeleteDialogComponent]
})
export class BillItemModule {}
