import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillFormPageComponent } from './containers/bill-form-page.component';
import { BillSearchPageComponent } from './containers/bill-search-page.component';

const routes: Routes = [
  {
    path: '',
    component: BillSearchPageComponent
  },
  {
    path: 'create',
    component: BillFormPageComponent
  },
  {
    path: 'update',
    component: BillFormPageComponent
  },
  {
    path: 'view',
    component: BillFormPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillItemRoutingModule {}
