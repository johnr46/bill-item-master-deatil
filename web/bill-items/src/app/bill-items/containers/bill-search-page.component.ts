import { HttpErrorResponse } from '@Angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs';

import { BillStateService } from '../service/bill-state.service';
import { BillService } from '../service/bill.service';
import { Bill } from '../types/bill';
import { BillCriteria } from '../types/billcriteria';

@Component({
  selector: 'app-bill-search-page',
  template: `
    <div style="text-align:right">
      <button mat-button color="primary" (click)="toCreate()">create</button>
    </div>
    <app-bill-form-search
      [formCriteria]="formCriteria"
      (billCri)="onSearch($event)"
      (Clear)="onClear($event)"
    ></app-bill-form-search>

    <app-bill-result-table
      [resultSearch]="resultSearch$ | async"
      (view)="toView($event)"
      (update)="toUpdate($event)"
    ></app-bill-result-table>
  `,
  styles: []
})
export class BillSearchPageComponent implements OnInit {
  resultSearch$: Observable<Bill[]>;
  formCriteria: BillCriteria;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private billService: BillService,
    private notificationsService: NotificationsService,
    private billStateService: BillStateService
  ) {}

  ngOnInit() {
    this.resultSearch$ = this.billStateService.bills$;
    this.formCriteria = this.billStateService.bilLCriteria;
  }

  onClear(e: Bill): void {
    this.billStateService.resetSearch();
  }
  toCreate(): void {
    this.billStateService.createing();
    //this.billStateService.resetSearch();
    this.router.navigate(['./create'], { relativeTo: this.activatedRoute });
  }
  toUpdate(bill: Bill): void {
    this.billStateService.update(bill);
    this.router.navigate(['./update'], { relativeTo: this.activatedRoute });
  }
  toView(bill: Bill): void {
    this.billStateService.view(bill);
    this.router.navigate(['./view'], { relativeTo: this.activatedRoute });
  }

  onSearch(billCri: BillCriteria): void {
    console.log(billCri);

    this.billStateService.updateCriteria(billCri);
    this.billService.search(billCri).subscribe(
      (result: Bill[]) => {
        this.billStateService.updateResult(result);
        if (result.length === 0) {
          this.notificationsService.info('แจ้งเตือน', 'ไม่พบข้อมูลที่ค้นหา');
        }
      },
      (httpError: HttpErrorResponse) => this.catchError(httpError)
    );
  }

  private catchError(httpError: HttpErrorResponse): void {
    console.log(httpError.status);
    const { status, error } = httpError;
    if (status === 420) {
      this.notificationsService.error('แจ้งเตือน', error, {
        timeOut: 0
      });
    }
  }
}
