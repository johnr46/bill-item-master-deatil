import { Component, OnInit } from '@angular/core';
import { Bill } from '../types/bill';
import { BillService } from '../service/bill.service';
import { HttpErrorResponse } from '@Angular/common/http/http';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import { BillStateService } from '../service/bill-state.service';
@Component({
  selector: 'app-bill-form-page',
  template: `
    <app-bill-form
      [setMode]="stateMode"
      [formBillFromPage]="formValue"
      (save)="onCreateBill($event)"
      (update)="onUpdate($event)"
    ></app-bill-form>
    <div style="padding-left:635px">
      <button mat-button color="warn" (click)="closePage()">close</button>
    </div>
  `,
  styles: []
})
export class BillFormPageComponent implements OnInit {
  formValue: Bill;
  stateMode = this.billStateService.stateMode;
  constructor(
    private billService: BillService,
    private notificationsService: NotificationsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private billStateService: BillStateService
  ) {}

  ngOnInit() {
    if (this.stateMode !== 'CREATE') {
      this.formValue = this.billStateService.bill || {};
    }
  }
  onUpdate(bill: Bill): void {
    this.billService.update(bill).subscribe(
      (result: Bill) => {
        this.billStateService.updateSuccess(result);
        console.log(result);
        this.notificationsService.success(
          'อัปเดตรายการ : ',
          bill.billCode + ' สำเร็จ'
        ),
          setTimeout(() => {}, 2000),
          this.closePage();
      },
      error => this.catchError(error)
    );
  }
  onCreateBill(bill: Bill): void {
    this.billService.create(bill).subscribe(
      (result: Bill) => {
        console.log(' create bill : ', result);
        this.billStateService.createSuccess(result);
        this.notificationsService.success(
          'สร้างรายการ : ',
          bill.billCode + ' สำเร็จ'
        );
        setTimeout(() => {}, 2000), this.closePage();
      },
      error => this.catchError(error)
    );
  }

  closePage(): void {
    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
  }
  private catchError(httpError: HttpErrorResponse): void {
    console.log(httpError.status);
    const { status, error } = httpError;
    if (status === 420) {
      this.notificationsService.error('แจ้งเตือน', error, {
        timeOut: 0
      });
    }
  }
}
