import { Item } from './item';

export interface Bill {
  id?: number;
  billCode?: string;
  active?: number;
  inactiveReason?: string;
  itemList?: Item[];
}
