export interface BillCriteria {
  billCode?: string;
  name?: string;
  qty?: number;
  price?: number;
}
