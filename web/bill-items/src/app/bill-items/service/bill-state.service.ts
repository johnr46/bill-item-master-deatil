import { Injectable } from '@angular/core';
import { Bill } from '../types/bill';
import { BillCriteria } from '../types/billcriteria';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BillStateService {
  bill: Bill = null;
  bilLCriteria: BillCriteria = {};
  stateMode: 'CREATE' | 'VIEW' | 'UPDATE';

  private _bills$ = new BehaviorSubject<Bill[]>([]);

  public get bills$(): Observable<Bill[]> {
    return this._bills$.asObservable();
  }

  createing(): void {
    this.stateMode = 'CREATE';

  }
  resetSearch(): void {
    this.bill = {};
    this._bills$.next([]);
  }

  updateSuccess(bills: Bill): void {
    const newval = (this._bills$.value || []).map(p =>
      p.id === bills.id ? bills : p
    );
    this._bills$.next(newval);
  }

  createSuccess(bill: Bill): void {
    const oldbill = this._bills$.value || [];
    this._bills$.next([bill, ...oldbill]);
  }

  updateCriteria(billCri: BillCriteria): void {
    this.bilLCriteria = billCri;
  }

  updateResult(bills: Bill[]): void {
    this._bills$.next(bills);
  }

  view(bills: Bill): void {
    this.bill = bills;
    this.stateMode = 'VIEW';
  }

  update(bills: Bill): void {
    this.stateMode = 'UPDATE';
    this.bill = bills;
  }

  constructor() {}
}
