import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bill } from '../types/bill';
import { BillCriteria } from '../types/billcriteria';

@Injectable({
  providedIn: 'root'
})
export class BillService {
  private baseUrl = 'http://localhost:8080/bill_item/bills';
  constructor(private http: HttpClient) {}

  create(bill: Bill): Observable<Bill> {
    return this.http.post<Bill>(this.baseUrl + '/create', bill);
  }

  search(billCri: BillCriteria): Observable<Bill[]> {
    return this.http.post<Bill[]>(this.baseUrl + '/find-by-criteria', billCri);
  }

  update(bill: Bill): Observable<Bill> {
    return this.http.post<Bill>(this.baseUrl + '/update', bill);
  }

  delete(bill: Bill): Observable<Bill> {
    return this.http.post<Bill>(this.baseUrl + '/delete', bill);
  }
}
