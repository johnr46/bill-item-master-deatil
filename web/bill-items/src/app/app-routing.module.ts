import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'bill-item',
    loadChildren: () =>
      import('./bill-items/bill-item.module').then(m => m.BillItemModule)
  },
  {
    path: '**',
    redirectTo: 'bill-item'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
